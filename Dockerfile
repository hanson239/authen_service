
FROM openjdk:11-jdk

WORKDIR /workspace/app

COPY mvnw .
COPY .mvn .mvn
COPY pom.xml .
COPY src src

RUN ./mvnw install -DskipTests
RUN jar -xf target/*.jar

ENTRYPOINT ["java","-cp","BOOT-INF/classes:BOOT-INF/lib/*","com.dds.skypos.authentication_service.AuthenticationServiceApplication"]
