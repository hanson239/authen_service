package com.dds.skypos.authentication_service.repository.generic;

import com.mongodb.client.MongoCollection;

public interface AbstractRepository<T, H> {
    MongoCollection<T> getCollection();
}
