package com.dds.skypos.authentication_service.repository;

import com.dds.skypos.authentication_service.model.LoginInfo;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface LoginInfoRepository extends MongoRepository<LoginInfo, String> {
    void deleteAllByUserLoginId(String id);
    void deleteAllByDeviceUuid(String uuid);
}
