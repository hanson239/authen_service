package com.dds.skypos.authentication_service.repository;

import com.dds.skypos.authentication_service.dto.res.UserDTO;
import com.dds.skypos.authentication_service.model.UserInfo;
import com.dds.skypos.authentication_service.model.constant.DictUserRoles;
import com.dds.skypos.authentication_service.service.component.MappingHelper;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import java.awt.print.Pageable;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface UserInfoRepository extends MongoRepository<UserInfo, String>, QuerydslPredicateExecutor<UserInfo> {
    Optional<UserInfo> findByEmailAndUserRole(String email, Integer userRole);
    Optional<UserInfo> findByDomainAndUserRole(String domain, Integer userRole);
    Optional<UserInfo> findByEmailAndUserRoleIn(String email, List <Integer> userRoles);
    Optional<UserInfo> findByEmailAndDomainAndDeleteFlagIsFalse(String email, String domain);
    Optional<UserInfo> findByEmailAndDomainAndUserRoleInAndDeleteFlagIsFalse(String email, String domain, List <Integer> userRoles);
    Optional<UserInfo> findByNumberIdAndDomainAndUserRoleInAndDeleteFlagIsFalse(Integer numberId, String domain, Collection<Integer> userRole);
    Optional<UserInfo> findByIdAndDeleteFlagIsFalseAndUserRole(String id, Integer userRole);
    Optional<UserInfo> findByIdAndDeleteFlagIsFalseAndUserRoleIn(String id, List <Integer> userRoles);
    Optional<UserInfo> findByIdAndDomainAndDeleteFlagIsFalseAndUserRoleIn(String id, String domain, List <Integer> userRoles);
    Optional<UserInfo> findByEmailAndDeleteFlagIsFalseAndUserRoleIn(String email, List <Integer> userRoles);
    Optional<UserInfo> findByDomainAndUserRoleAndDeleteFlagFalse(String domain, Integer userRole);
    Optional<UserInfo> findByAccountIdAndNumberIdAndUserRoleInAndDeleteFlagIsFalse(String accountId, Integer numberId, List<Integer> userRoles);
    Optional<UserInfo> findByAccountIdAndEmail(String accountId, String email );
    Optional<UserInfo> findByAccountIdAndNumberId(String accountId, Integer numberId);
    Optional<UserInfo> findByAccountIdAndNumberIdAndDeleteFlagIsFalse(String accountId, Integer numberId);

}


