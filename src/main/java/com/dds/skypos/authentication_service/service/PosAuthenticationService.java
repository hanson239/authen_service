package com.dds.skypos.authentication_service.service;

import com.dds.skypos.authentication_service.dto.req.*;
import com.dds.skypos.authentication_service.dto.res.AuthorizationTokenDTO;
import com.dds.skypos.authentication_service.dto.res.LoginDTO;
import com.dds.skypos.authentication_service.dto.res.ResetPasswordTokenDTO;
import com.dds.skypos.authentication_service.dto.res.UserDTO;
import com.dds.skypos.authentication_service.exception.*;
import com.dds.skypos.authentication_service.exception.common.SysException;
import com.dds.skypos.authentication_service.model.*;
import com.dds.skypos.authentication_service.model.constant.DictUserRoles;
import com.dds.skypos.authentication_service.repository.LoginInfoRepository;
import com.dds.skypos.authentication_service.repository.UserInfoRepository;
import com.dds.skypos.authentication_service.security.SecurityContextManager;
import com.dds.skypos.authentication_service.service.component.MappingHelper;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@RequiredArgsConstructor
@Transactional
public class PosAuthenticationService {

    private final JwtService jwtService;
    private final MailService mailService;
    private final MappingHelper mappingHelper;
    private final UserInfoRepository userInfoRepository;
    private final PasswordEncoder passwordEncoder;
    private final LoginInfoRepository loginInfoRepository;
    private final SecurityContextManager securityContextManager;
    private final OTPService otpService;

    @Transactional(readOnly = true)
    public LoginInfo parseAccessToken(String accessToken) {
        String loginId = jwtService.decodeAccessToken(accessToken);
        return loginInfoRepository.findById(loginId)
                .map(loginInfo -> mappingHelper.map(loginInfo, LoginInfo.class))
                .orElseThrow(InvalidAccessTokenException::new);
    }

    public void preRegister(PreRegisterAccountDTO requestDTO) {
        validateNewPassword(requestDTO.getPassword());
        validateEmailRegister(requestDTO.getEmail());
        validateDomain(requestDTO.getDomain());
        String otp = otpService.generateRegisterAccountOTP(requestDTO.getEmail());
        mailService.sendRegisterAccount(requestDTO.getEmail(), requestDTO.getFullName(), otp);
    }

    public UserDTO register(RegisterAccountDTO requestDTO) {
        validateNewPassword(requestDTO.getPassword());
        validateEmailRegister(requestDTO.getEmail());
        validateDomain(requestDTO.getDomain());
        validateOTPRegister(requestDTO.getOtp(), requestDTO.getEmail());
        return Optional.of(createStoreAccountInfo(requestDTO))
                .map(userInfoRepository::save)
                .map(otpService::deleteRegisterAccountOTP)
                .map(userInfo -> mappingHelper.map(userInfo, UserDTO.class))
                .orElseThrow(() -> new SysException("Cannot create user."));
    }

    public LoginDTO loginByEmail(LoginByEmailDTO requestDTO) {
        checkAccountStoreIsActive(requestDTO.getDomain());
        return userInfoRepository.findByEmailAndDomainAndUserRoleInAndDeleteFlagIsFalse(requestDTO.getEmail(), requestDTO.getDomain(),
                List.of(DictUserRoles.ROLE_STORE_ACCOUNT.getValue(), DictUserRoles.ROLE_STORE_ADMIN.getValue()))
                .map(userInfo -> validatePassword(userInfo, requestDTO.getPassword()))
                .map(userInfo -> LoginInfo.of(mappingHelper.map(userInfo, UserLogin.class)))
                .map(loginInfoRepository::save)
                .map(this::createLoginResponse)
                .orElseThrow(EntityNotFoundException::new);
    }

    public LoginDTO loginById(LoginByIdDTO requestDTO) {
        checkAccountStoreIsActive(requestDTO.getDomain());
        return userInfoRepository.findByNumberIdAndDomainAndUserRoleInAndDeleteFlagIsFalse(requestDTO.getNumberId(), requestDTO.getDomain(),
                List.of(DictUserRoles.ROLE_STORE_ADMIN.getValue(), DictUserRoles.ROLE_CASHIER.getValue()))
                //TODO: call Store Service to validate permission of this user with this storeCode
                .map(userInfo -> validatePassCode(userInfo, requestDTO.getPassCode()))
                .map(userInfo -> LoginInfo.of(mappingHelper.map(userInfo, UserLogin.class), mappingHelper.map(requestDTO.getDevice(), DeviceLogin.class)))
                .map(this::clearPreviousLoginInfoOfDevice)
                .map(loginInfoRepository::save)
                .map(this::createLoginResponse)
                .orElseThrow(EntityNotFoundException::new);
    }

    public UserDTO createEmployee(EmployeeCreateDTO requestDTO) {
        UserInfo userLogin = securityContextManager.getCurrentLoginUserInfo();
        validateCreateEmployee(requestDTO, userLogin);
        return Optional.of(createEmployeeInfo(requestDTO))
                .map(userInfoRepository::save)
                .map(user -> mappingHelper.map(user, UserDTO.class))
                .orElseThrow(() -> new SysException("Cannot create user."));
    }

    //TODO: Call to store service to save employee in sore.
    private UserInfo saveEmployeeInStoreService(String id, String accountId, String storeCodeForCashier,
                                                Integer numberId, Integer userRole,
                                                String fullName, String email) {
        return new UserInfo();
    }

    private void validateCreateEmployee(EmployeeCreateDTO requestDTO, UserInfo user) {
        if (!Objects.equals(user.getRole(), DictUserRoles.ROLE_STORE_ACCOUNT)
                && !Objects.equals(user.getRole(), DictUserRoles.ROLE_SYSTEM_ADMIN)) {
            throw new AccessDeniedExceptions();
        }
        validateNewPassword(requestDTO.getPassword());
        if (Objects.equals(requestDTO.getUserRole(), DictUserRoles.getByValue(user.getUserRole()))) {
            throw new AccessDeniedExceptions();
        }
        validateEmail(requestDTO.getEmail(), user);
        validateNumberId(requestDTO.getNumberId(), user);

    }

    private void validateNumberId(Integer numberId, UserInfo user) {
        userInfoRepository.findByAccountIdAndNumberId(user.getAccountId(), numberId)
                .ifPresent(userInfo -> {
                    throw new IdExistedException();
                });
    }

    private void validateEmail(String email, UserInfo user) {
        userInfoRepository.findByAccountIdAndEmail(user.getAccountId(), email)
                .ifPresent(userInfo -> {
                    throw new EmailExistedException();
                });

    }

    public Page<UserDTO> searchEmployee(EmployeeSearchDTO requestDTO, Pageable pageable) {
        UserInfo userLogin = securityContextManager.getCurrentLoginUserInfo();
        if (DictUserRoles.valueOf(requestDTO.getUserRole()).getValue() <= userLogin.getUserRole()) {
            throw new AccessDeniedExceptions();
        }
        //TODO call Store Service to get list store of store admin and remove this line
        List<String> storesCode = new ArrayList<>();
        Predicate predicate = QUserInfo.userInfo.accountId.equalsIgnoreCase(userLogin.getAccountId())
                .and(QUserInfo.userInfo.userRole.eq(DictUserRoles.valueOf(requestDTO.getUserRole()).getValue()))
                .and((CollectionUtils.isNotEmpty(storesCode)) ? QUserInfo.userInfo.storeCodeForCashier.in(storesCode) : null)
                .and(QUserInfo.userInfo.fullName.containsIgnoreCase(requestDTO.getKeySearch())
                        .or((StringUtils.isNumeric(requestDTO.getKeySearch()) ? QUserInfo.userInfo.numberId.eq(Integer.parseInt(requestDTO.getKeySearch())) : null)));
        return userInfoRepository.findAll(predicate, pageable)
                .map(userInfo -> mappingHelper.map(userInfo, UserDTO.class));
    }

    public void deleteEmployee(Integer numberId) {
        UserInfo userInfo = securityContextManager.getCurrentLoginUserInfo();
        userInfoRepository.findByAccountIdAndNumberIdAndUserRoleInAndDeleteFlagIsFalse(userInfo.getAccountId(), numberId,
                List.of(DictUserRoles.ROLE_CASHIER.getValue(), DictUserRoles.ROLE_STORE_ADMIN.getValue()))
                .map((user) -> {
                    if (Objects.equals(userInfo.getUserRole(), user.getUserRole())) {
                        throw new AccessDeniedExceptions();
                    }
                    return user;
                })
                .map(this::clearAllPreviousLoginInfoByUser)
//                TODO: Call to Store service
//                .map(userInfo1 -> {
//                    deleteEmployeeInStore(userInfo);
//                    return userInfo;
//                })
                .map(this::setDeleteFlagTrue)
                .map(userInfoRepository::save)
                .orElseThrow(EntityNotFoundException::new);
    }

    private void deleteEmployeeInStore(UserInfo userInfo) {
        //TODO: Call to Store Service and delete employee in store
    }

    public void logout() {
        LoginInfo loginInfo = securityContextManager.getCurrentLoginInfo();
        loginInfoRepository.delete(loginInfo);
    }

    public void storeAccountLogoutFromPos(StoreAccountLogoutFromPosDTO requestDTO) {
        validatePassword(securityContextManager.getCurrentLoginUserInfo(), requestDTO.getPassword());
        LoginInfo loginInfo = securityContextManager.getCurrentLoginInfo();
        loginInfoRepository.delete(loginInfo);
    }

    public void changePassword(ChangePasswordDTO requestDTO) {
        validateNewPassword(requestDTO.getNewPassword());
        Optional.ofNullable(securityContextManager.getCurrentLoginUserInfo())
                .map(userInfo -> validatePassword(userInfo, requestDTO.getCurrentPassword()))
                .map(userInfo -> checkNewPasswordExistInPasswordHistory(userInfo, requestDTO.getNewPassword()))
                .map(userInfo -> updatePasswordAndPasswordHistory(userInfo, requestDTO.getNewPassword()))
                .map(this::clearAllPreviousLoginInfoByUser)
                .map(userInfoRepository::save)
                .orElseThrow(EntityNotFoundException::new);
    }

    @Transactional(readOnly = true)
    public void forgotPassword(ForgotPasswordDTO requestDTO) {
        checkAccountStoreIsActive(requestDTO.getDomain());
        userInfoRepository.findByEmailAndDomainAndDeleteFlagIsFalse(requestDTO.getEmail(), requestDTO.getDomain())
                .ifPresent(this::sendEmailForgotPassword);
    }

    @Transactional(readOnly = true)
    public ResetPasswordTokenDTO validateOTPResetPassword(ValidateOTPResetPasswordDTO requestDTO) {
        checkAccountStoreIsActive(requestDTO.getDomain());
        return userInfoRepository.findByEmailAndDomainAndDeleteFlagIsFalse(requestDTO.getEmail(), requestDTO.getDomain())
                .filter(userInfo -> Objects.equals(
                        otpService.getForgotPasswordOTP(userInfo),
                        requestDTO.getOtp()
                ))
                .map(otpService::deleteForgotPasswordOTP)
                .map(jwtService::generateResetPasswordToken)
                .map(ResetPasswordTokenDTO::new)
                .orElseThrow(EntityNotFoundException::new);
    }

    public void resetPassword(ResetPasswordDTO requestDTO) {
        validateNewPassword(requestDTO.getNewPassword());
        checkAccountStoreIsActive(requestDTO.getDomain());
        String userId = jwtService.decodeResetPasswordToken(requestDTO.getResetPasswordToken());
        userInfoRepository.findByIdAndDomainAndDeleteFlagIsFalseAndUserRoleIn(userId, requestDTO.getDomain(),
                List.of(DictUserRoles.ROLE_STORE_ACCOUNT.getValue(), DictUserRoles.ROLE_STORE_ADMIN.getValue(), DictUserRoles.ROLE_CASHIER.getValue()))
                .map(userInfo -> checkNewPasswordExistInPasswordHistory(userInfo, requestDTO.getNewPassword()))
                .map(userInfo -> updatePasswordAndPasswordHistory(userInfo, requestDTO.getNewPassword()))
                .map(userInfoRepository::save)
                .orElseThrow(EntityNotFoundException::new);
    }

    public AuthorizationTokenDTO generateAuthorizationToken() {
        return new AuthorizationTokenDTO(
                jwtService.generateAuthorizationToken(securityContextManager.getCurrentLoginUserInfo()));
    }

    private void validateEmailRegister(String email) {
        userInfoRepository.findByEmailAndUserRole(email, DictUserRoles.ROLE_STORE_ACCOUNT.getValue())
                .ifPresent(userInfo -> {
                    throw new EmailExistedException();
                });
    }

    private void validateDomain(String domain) {

        Optional.ofNullable(domain)
                .filter(this::isValidDomainLength)
                .filter(this::isValidQuantityOfSpecialCharacter)
                .filter(this::isStartOrEndByAlphabetLowerCaseCharacter)
                .filter(this::isContainValidCharacter)
                .orElseThrow(InvalidDomainException::new);

        userInfoRepository.findByDomainAndUserRole(domain, DictUserRoles.ROLE_STORE_ACCOUNT.getValue())
                .ifPresent(userInfo -> {
                    throw new DomainExistedException();
                });
    }

    private boolean isValidDomainLength(String domain) {
        return StringUtils.length(domain) >= 2 && StringUtils.length(domain) <= 10;
    }

    private Boolean isValidQuantityOfSpecialCharacter(String domain) {
        return StringUtils.countMatches(domain, '-') + StringUtils.countMatches(domain, '.') <= 1;
    }

    private Boolean isStartOrEndByAlphabetLowerCaseCharacter(String domain) {
        return Character.isLowerCase(domain.charAt(0)) && Character.isLowerCase(domain.charAt(domain.length() - 1));
    }

    private Boolean isContainValidCharacter(String domain) {
        return domain.chars().allMatch(character -> Character.isLowerCase(character) || Objects.equals(character, (int) '.') ||  Objects.equals(character, (int) '-'));
    }

    private void validateNewPassword(String password) {
        validatePasswordLength(password);

        AtomicInteger alphabetCharacterCount = new AtomicInteger();
        AtomicInteger numericCharacterCount = new AtomicInteger();
        AtomicInteger specialCharacterCount = new AtomicInteger();

        password.chars()
                .map(character -> countAlphabetCharacter(character, alphabetCharacterCount))
                .map(character -> countNumericCharacter(character, numericCharacterCount))
                .forEach(character -> countSpecialCharacter(character, specialCharacterCount));

        if (isMissingRequiredCharacter(alphabetCharacterCount.get(), numericCharacterCount.get(), specialCharacterCount.get())) {
            throw new InvalidPasswordException();
        }
    }

    private void validatePasswordLength(String password) {
        if (StringUtils.length(password) < 8) {
            throw new InvalidPasswordException();
        }
    }

    private Integer countAlphabetCharacter(Integer character, AtomicInteger alphabetCharacterCount) {
        if (Character.isLetter(character)) {
            alphabetCharacterCount.getAndIncrement();
        }
        return character;
    }

    private Integer countNumericCharacter(Integer character, AtomicInteger numericCharacterCount) {
        if (Character.isDigit(character)) {
            numericCharacterCount.getAndIncrement();
        }
        return character;
    }

    private void countSpecialCharacter(Integer character, AtomicInteger specialCharacterCount) {
        if (!Character.isLetterOrDigit(character)) {
            specialCharacterCount.getAndIncrement();
        }
    }

    private Boolean isMissingRequiredCharacter(int alphabetCharacterCount, int numericCharacterCount, int specialCharacterCount) {
        return Objects.equals(alphabetCharacterCount, 0) || Objects.equals(numericCharacterCount, 0) || Objects.equals(specialCharacterCount, 0);
    }

    private void validateOTPRegister(String requestOTP, String email) {
        if (!Objects.equals(requestOTP, otpService.getRegisterAccountOTP(email))) {
            throw new InvalidOTPException();
        }
    }

    private UserInfo createStoreAccountInfo(RegisterAccountDTO requestDTO) {
        UserInfo userInfo = mappingHelper.map(requestDTO, UserInfo.class);
        userInfo.setPassword(passwordEncoder.encode(requestDTO.getPassword()));
        userInfo.setNumberId(1);
        userInfo.setDeleteFlag(Boolean.FALSE);
        userInfo.setPasswordHistory(List.of(userInfo.getPassword()));
        userInfo.setUserRole(DictUserRoles.ROLE_STORE_ACCOUNT.getValue());
        //TODO:Call Store Service to create Account, receive accountId, use receive accountId instead of random
        userInfo.setAccountId(RandomStringUtils.randomNumeric(10));
        return userInfo;
    }

    private void checkAccountStoreIsActive(String domain) {
        userInfoRepository.findByDomainAndUserRoleAndDeleteFlagFalse(domain, DictUserRoles.ROLE_STORE_ACCOUNT.getValue())
                .orElseThrow(EntityNotFoundException::new);
    }

    private UserInfo validatePassword(UserInfo userInfo, String requestPassword) {
        if (!passwordEncoder.matches(requestPassword, userInfo.getPassword())) {
            throw new PasswordNotMatchException();
        }
        return userInfo;
    }

    private UserInfo validatePassCode(UserInfo userInfo, String requestPassCode) {
        if (!passwordEncoder.matches(requestPassCode, userInfo.getPassCode())) {
            throw new PasswordNotMatchException();
        }
        return userInfo;
    }

    private LoginInfo clearPreviousLoginInfoOfDevice(LoginInfo loginInfo) {
        loginInfoRepository.deleteAllByDeviceUuid(loginInfo.getDevice().getUuid());
        return loginInfo;
    }

    private LoginDTO createLoginResponse(LoginInfo loginInfo) {
        return new LoginDTO(jwtService.generateAccessToken(loginInfo.getId()), loginInfo.getUserLogin().getRole());
    }

    private UserInfo createEmployeeInfo(EmployeeCreateDTO requestDTO) {
        String accountId = securityContextManager.getCurrentLoginUserInfo().getAccountId();
        UserInfo userInfo = mappingHelper.map(requestDTO, UserInfo.class);
        userInfo.setUserRole(DictUserRoles.valueOf(requestDTO.getUserRole()).getValue());
        if (Objects.equals(userInfo.getUserRole(), DictUserRoles.ROLE_STORE_ADMIN.getValue())) {
            userInfo.setStoreCodeForCashier(null);
        }
        userInfo.setPassword(passwordEncoder.encode(requestDTO.getPassword()));
        userInfo.setDeleteFlag(Boolean.FALSE);
        userInfo.setPasswordHistory(Collections.singletonList(userInfo.getPassword()));
        userInfo.setAccountId(accountId);
        return userInfo;
    }

    private UserInfo setDeleteFlagTrue(UserInfo userInfo) {
        userInfo.setDeleteFlag(Boolean.TRUE);
        return userInfo;
    }

    private UserInfo clearAllPreviousLoginInfoByUser(UserInfo userInfo) {
        loginInfoRepository.deleteAllByUserLoginId(userInfo.getId());
        return userInfo;
    }

    private void sendEmailForgotPassword(UserInfo userInfo) {
        String otp = otpService.generateForgotPasswordOTP(userInfo);
        mailService.sendForgotPassword(userInfo.getEmail(), userInfo.getFullName(), otp);
    }

    private UserInfo checkNewPasswordExistInPasswordHistory(UserInfo userInfo, String newPassword) {
        if (userInfo.getPasswordHistory().stream()
                .anyMatch(password -> passwordEncoder.matches(newPassword, password))) {
            throw new UsedPasswordException();
        }
        return userInfo;
    }

    private UserInfo updatePasswordAndPasswordHistory(UserInfo userInfo, String newPassword) {
        userInfo.setPassword(passwordEncoder.encode(newPassword));
        userInfo.getPasswordHistory().add(0, userInfo.getPassword());
        if (userInfo.getPasswordHistory().size() > 4)
            userInfo.setPasswordHistory(userInfo.getPasswordHistory().subList(0, 4));
        return userInfo;
    }

    public UserDTO updateEmployee(EmployeeUpdateDTO requestDTO, Integer numberId) {
        UserInfo user = securityContextManager.getCurrentLoginUserInfo();
        validateNumberId(requestDTO.getNumberId(), user);
        return userInfoRepository.findByAccountIdAndNumberIdAndDeleteFlagIsFalse(user.getAccountId(), numberId)
                .map(userInfo -> {
                    if (userInfo.getUserRole() <= user.getUserRole()) {
                        throw new AccessDeniedExceptions();
                    }
                    if (Objects.equals(userInfo.getUserRole(), DictUserRoles.ROLE_STORE_ADMIN.getValue())) {
                        requestDTO.setStoreCodeForCashier(null);
                    }
                    mappingHelper.copyProperties(requestDTO, userInfo);
                    return userInfo;
                })
                .map(userInfoRepository::save)
                .map(userInfo -> mappingHelper.map(userInfo, UserDTO.class))
                .orElseThrow(EntityNotFoundException::new);
    }
}
