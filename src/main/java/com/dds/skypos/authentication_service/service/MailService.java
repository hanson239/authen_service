package com.dds.skypos.authentication_service.service;

import com.dds.skypos.authentication_service.service.component.EmailTemplate;
import com.dds.skypos.authentication_service.service.component.I18nMessageHelper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

@Service
@Slf4j
@RequiredArgsConstructor
public class MailService {

    private final JavaMailSender javaMailSender;
    private final I18nMessageHelper messageHelper;
    private final TemplateEngine templateEngine;

    @Value("${spring.mail.username}")
    private String mailFrom;
    @Value("${spring.mail.display.name}")
    private String displayName;

    @Async
    @SneakyThrows({MessagingException.class,UnsupportedEncodingException.class})
    public void sendForgotPassword(String email, String name, String otp) {
        Context context = new Context(new Locale("vi"));
        context.setVariable("name", name);
        context.setVariable("otp", otp);
        String content = templateEngine.process(messageHelper.getMessage(EmailTemplate.FORGOT_PASSWORD.getTemplateKey(), context.getLocale()), context);
        String subject = "Skypos Forgot Password";
        sendEmail(email, subject, content, false, true);
    }

    @Async
    @SneakyThrows({MessagingException.class,UnsupportedEncodingException.class})
    public void sendRegisterAccount(String email, String name, String otp) {
        Context context = new Context(new Locale("vi"));
        context.setVariable("name", name);
        context.setVariable("otp", otp);
        String content = templateEngine.process(messageHelper.getMessage(EmailTemplate.REGISTER_ACCOUNT.getTemplateKey(), context.getLocale()), context);
        String subject = "Skypos Register Account";
        sendEmail(email, subject, content, false, true);
    }

    private void sendEmail(String receiver, String subject, String content, boolean isMultiPart, boolean isHtml) throws MessagingException, UnsupportedEncodingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        setMessageProperties(mimeMessage, receiver, subject, content, isMultiPart, isHtml);
        javaMailSender.send(mimeMessage);
    }

    private void setMessageProperties(MimeMessage mimeMessage, String receiver, String subject, String content, boolean isMultiPart, boolean isHtml) throws MessagingException, UnsupportedEncodingException {
        MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultiPart, StandardCharsets.UTF_8.name());
        message.setFrom(mailFrom, displayName);
        message.setTo(receiver);
        message.setSubject(subject);
        message.setText(content, isHtml);
    }
}