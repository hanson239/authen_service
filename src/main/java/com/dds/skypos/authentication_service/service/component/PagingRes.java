package com.dds.skypos.authentication_service.service.component;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
public class PagingRes {
    private int pageSize;
    private int pageNum;
    private long total;
    private List<? extends Object> pageData;

    protected PagingRes(Page<? extends Object> page) {
        this.pageData = page.getContent();
        this.total = page.getTotalElements();
        this.pageNum = page.getNumber() + 1;
        this.pageSize = page.getSize();
    }

    public static PagingRes of (Page<? extends Object> page) {
        return new PagingRes(page);
    }
}
