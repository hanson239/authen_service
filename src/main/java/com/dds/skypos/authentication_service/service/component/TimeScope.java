package com.dds.skypos.authentication_service.service.component;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.apache.logging.log4j.util.Strings;
import utils.SystemTimeUtils;
import utils.TimeRange;

import java.util.Objects;

@Data
public class TimeScope {

    @ApiParam("Quick Option: CURRENT_MONTH, CURRENT_QUARTER, CURRENT_YEAR. This option overwrite 'specific' options")
    @ApiModelProperty("Quick Option: CURRENT_MONTH, CURRENT_QUARTER, CURRENT_YEAR. This option overwrite 'specificOpt' options")
    private QuickOption opt;

    @ApiParam(value = "Option to select specific time scope", hidden = true)
    @ApiModelProperty(value = "Option to select specific time scope", hidden = true)
    private SpecificOption specific;

    public enum QuickOption {
        CURRENT_MONTH, CURRENT_QUARTER, CURRENT_YEAR,
    }

    @Data
    public static class SpecificOption {

        @ApiParam("year in format: yyyy")
        @ApiModelProperty("year in format: yyyy")
        private String year;

        @ApiParam("quarter in format: yyyy_Q1 | yyyy_Q2 | yyyy_Q3 | yyyy_Q4. This option will overwrite 'year' option")
        @ApiModelProperty("quarter in format: yyyy_Q1 | yyyy_Q2 | yyyy_Q3 | yyyy_Q4. This option will overwrite 'year' option\"")
        private String quarter;

        @ApiParam("month in format: yyyyMM. This option will overwrite 'quarter', 'year' options")
        @ApiModelProperty("month in format: yyyyMM. This option will overwrite 'quarter', 'year' options")
        private String month;
    }

    public static TimeRange determineTimeRange(TimeScope time) {
        if (Objects.nonNull(time.getOpt())) {
            switch (time.getOpt()) {
                case CURRENT_MONTH:
                    return SystemTimeUtils.getLocalCurrentMonthTimeRange();
                case CURRENT_QUARTER:
                    return SystemTimeUtils.getLocalCurrentQuarterTimeRange();
                case CURRENT_YEAR:
                default:
                    return SystemTimeUtils.getLocalCurrentYearTimeRange();
            }
        } else if (Objects.nonNull(time.getSpecific())) {
            if (Strings.isNotBlank(time.getSpecific().getMonth())) {
                return SystemTimeUtils.parseTimeRangeFromMonthString(time.getSpecific().getMonth());
            } else if (Strings.isNotBlank(time.getSpecific().getQuarter())) {
                return SystemTimeUtils.parseTimeRangeFromQuarterString(time.getSpecific().getQuarter());
            } else if (Strings.isNotBlank(time.getSpecific().getYear())) {
                return SystemTimeUtils.parseTimeRangeFromYearString(time.getSpecific().getYear());
            } else {
                throw new IllegalArgumentException("Invalid input data");
            }
        } else {
            throw new IllegalArgumentException("Invalid input data");
        }
    }
}
