package com.dds.skypos.authentication_service.service.component;

import lombok.Getter;
@Getter
public enum EmailTemplate {
    FORGOT_PASSWORD("templates.forgot_password"),
    REGISTER_ACCOUNT("templates.register_account");
    EmailTemplate(String templateKey) {
        this.templateKey = templateKey;
    }
    private final String templateKey;
}