package com.dds.skypos.authentication_service.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.dds.skypos.authentication_service.exception.InvalidAccessTokenException;
import com.dds.skypos.authentication_service.exception.InvalidResetPasswordTokenException;
import com.dds.skypos.authentication_service.exception.common.SysException;
import com.dds.skypos.authentication_service.model.UserInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.sql.Date;
import java.time.Instant;

@Service
@Slf4j
public class JwtService {
    private static final String LOGIN_ID = "login_id";
    private static final String USER_ID = "id";
    private static final String ACCOUNT_ID = "accountId";
    private static final String USER_ROLE = "userRole";
    private static final String USER_EMAIL = "email";
    private static final String USER_FULLNAME = "fullName";

    @Value("${jwt.secret}")
    private String JWT_SECRET;
    @Value("${jwt.expirationTime}")
    private long JWT_EXPIRATION_TIME;
    private Algorithm algorithm;

    @PostConstruct
    public void init() {
        algorithm = Algorithm.HMAC256(JWT_SECRET);
    }

    public String generateAccessToken(String loginInfo) {
        try {
            return JWT.create()
                    .withClaim(LOGIN_ID, loginInfo)
                    .sign(algorithm);
        } catch (JWTCreationException exception) {
            throw new SysException("Cannot generate Access Token", exception);
        }
    }

    public String decodeAccessToken(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim(LOGIN_ID).asString();
        } catch (JWTDecodeException exception) {
            throw new InvalidAccessTokenException();
        }
    }

    public String generateAuthorizationToken(UserInfo userInfo) {
        try {
            return JWT.create()
                    .withClaim(USER_ID, userInfo.getId())
                    .withClaim(ACCOUNT_ID, userInfo.getAccountId())
                    .withClaim(USER_EMAIL, userInfo.getEmail())
                    .withClaim(USER_ROLE, userInfo.getRole().getRole())
                    .withClaim(USER_FULLNAME, userInfo.getFullName())
                    .sign(algorithm);
        } catch (JWTCreationException exception) {
            throw new SysException("Cannot generate Authorization Token", exception);
        }
    }

    public String generateResetPasswordToken(UserInfo userInfo) {
        try {
            return JWT.create()
                    .withClaim(USER_ID, userInfo.getId())
                    .withExpiresAt(Date.from(Instant.now().plusSeconds(1800)))
                    .sign(algorithm);
        } catch (JWTCreationException exception) {
            throw new SysException("Cannot generate reset password token", exception);
        }
    }

    public String decodeResetPasswordToken(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            timeValidate(jwt);
            return jwt.getClaim(USER_ID).asString();
        } catch (JWTDecodeException exception) {
            throw new InvalidResetPasswordTokenException();
        }
    }

    private void timeValidate(DecodedJWT jwt) {
        if (jwt.getExpiresAt().toInstant().isBefore(Instant.now())) {
            throw new InvalidResetPasswordTokenException();
        }
    }
}
