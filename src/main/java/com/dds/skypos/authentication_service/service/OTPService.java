package com.dds.skypos.authentication_service.service;

import com.dds.skypos.authentication_service.exception.common.SysException;
import com.dds.skypos.authentication_service.model.UserInfo;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@Service
public class OTPService {

    public static final Integer FORGOT_PASSWORD_EXPIRE_MINUTES = 30;
    public static final Integer REGISTER_EXPIRE_MINUTES = 2;

    private LoadingCache<String, String> forgotPasswordOTP;
    private LoadingCache<String, String> registerAccountOTP;

    public OTPService() {
        forgotPasswordOTP = CacheBuilder.newBuilder()
                .expireAfterWrite(FORGOT_PASSWORD_EXPIRE_MINUTES, TimeUnit.MINUTES).build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String s) throws Exception {
                        return "0";
                    }
                });
        registerAccountOTP = CacheBuilder.newBuilder()
                .expireAfterWrite(REGISTER_EXPIRE_MINUTES, TimeUnit.MINUTES).build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String s) throws Exception {
                        return "0";
                    }
                });
    }

    public String generateForgotPasswordOTP(UserInfo userInfo) {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        String otp = StringUtils.EMPTY;
        forgotPasswordOTP.invalidate(otp);
        do {
            otp = String.valueOf(
                    random.nextLong(10_000L, 100_000L)
            );
        } while (Objects.nonNull(forgotPasswordOTP.getIfPresent(otp)));
        forgotPasswordOTP.put(userInfo.getEmail(), otp);
        return otp;
    }

    public String getForgotPasswordOTP(UserInfo userInfo) {
        try {
            return forgotPasswordOTP.get(userInfo.getEmail());
        } catch (ExecutionException e) {
            throw new SysException(e.getMessage());
        }
    }

    public UserInfo deleteForgotPasswordOTP(UserInfo userInfo) {
        forgotPasswordOTP.invalidate(userInfo.getEmail());
        return userInfo;
    }

    public String generateRegisterAccountOTP(String email) {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        String otp = StringUtils.EMPTY;
        registerAccountOTP.invalidate(otp);
        do {
            otp = String.valueOf(
                    random.nextLong(10_000L, 100_000L)
            );
        } while (Objects.nonNull(registerAccountOTP.getIfPresent(otp)));
        registerAccountOTP.put(email, otp);
        return otp;
    }

    public String getRegisterAccountOTP(String email) {
        try {
            return registerAccountOTP.get(email);
        } catch (ExecutionException e) {
            throw new SysException(e.getMessage());
        }
    }

    public UserInfo deleteRegisterAccountOTP(UserInfo userInfo) {
        registerAccountOTP.invalidate(userInfo.getEmail());
        return userInfo;
    }

}
