package com.dds.skypos.authentication_service.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.Instant;

@Data
public abstract class AbstractModel {

    @CreatedDate
    protected Instant createdDate;
    @LastModifiedDate
    protected Instant modifiedDate;

}
