package com.dds.skypos.authentication_service.model;

import lombok.Data;

@Data
public class DeviceLogin {

    private String uuid;
    private String macAddress;
    private String deviceOS;

}
