package com.dds.skypos.authentication_service.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@Document
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LoginInfo {

    @Id
    private String id;
    @CreatedDate
    private Instant loginTime;
    private UserLogin userLogin;
    private DeviceLogin device;

    static public LoginInfo of (UserLogin userLogin, DeviceLogin device) {
        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setUserLogin(userLogin);
        loginInfo.setDevice(device);
        return loginInfo;
    }

    static public LoginInfo of (UserLogin userLogin){
        LoginInfo loginInfo = new LoginInfo();
        loginInfo.setUserLogin(userLogin);
        return loginInfo;
    }

}
