package com.dds.skypos.authentication_service.model;

import com.dds.skypos.authentication_service.exception.common.SysException;
import com.dds.skypos.authentication_service.model.constant.DictUserRoles;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Document
public class UserInfo extends AbstractModel implements UserDetails {

    @Id
    private String id;
    private Integer numberId;
    private String accountId;
    private String email;
    private String storeCodeForCashier;
    private String domain;
    @ToString.Exclude
    private String password;
    private String passCode;
    private String fullName;
    private String phoneNumber;
    private Integer userRole;
    private List<String> passwordHistory;
    private Boolean deleteFlag;

    public DictUserRoles getRole(){
        return DictUserRoles.getByValue(userRole).get();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = DictUserRoles.getByValue(this.getUserRole())
                .map(DictUserRoles::getRole)
                .orElseThrow(() -> new SysException("Role does not exist"));
        return Collections.singleton(new SimpleGrantedAuthority(role));
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
