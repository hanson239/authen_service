package com.dds.skypos.authentication_service.model.constant;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Getter
public enum DictUserRoles {
    ROLE_SUPER_SYSTEM_ADMIN(1, "ROLE_SUPER_SYSTEM_ADMIN"),
    ROLE_SYSTEM_ADMIN(2, "ROLE_SYSTEM_ADMIN"),
    ROLE_STORE_ACCOUNT(3, "ROLE_STORE_ACCOUNT"),
    ROLE_STORE_ADMIN(4, "ROLE_STORE_ADMIN"),
    ROLE_CASHIER(5, "ROLE_CASHIER");

    private static final Map<Integer, DictUserRoles> USER_ROLE_MAP = new HashMap<>();

    static {
        for (DictUserRoles e : values()) {
            USER_ROLE_MAP.put(e.getValue(), e);
        }
    }

    private final Integer value;
    private final String role;

    private DictUserRoles(int value, String role) {
        this.value = value;
        this.role = role;
    }

    public static Optional<DictUserRoles> getByValue(int value) {
        return Optional.of(USER_ROLE_MAP.get(value));
    }
}
