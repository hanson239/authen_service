package com.dds.skypos.authentication_service.model;

import com.dds.skypos.authentication_service.exception.common.SysException;
import com.dds.skypos.authentication_service.model.constant.DictUserRoles;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collection;
import java.util.Collections;

@Data
public class UserLogin {

    private String id;
    private String email;
    private Integer userRole;

    public String getRole(){
        return DictUserRoles.getByValue(userRole).get().getRole();
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        String role = DictUserRoles.getByValue(this.getUserRole())
                .map(DictUserRoles::getRole)
                .orElseThrow(() -> new SysException("Role does not exist"));
        return Collections.singleton(new SimpleGrantedAuthority(role));
    }

}
