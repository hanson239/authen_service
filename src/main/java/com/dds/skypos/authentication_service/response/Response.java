package com.dds.skypos.authentication_service.response;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class Response {
    protected String message;
    protected Object data;

    public static Response of (String message, Object data) {
        return new Response(message, data);
    }

}
