package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;
import com.dds.skypos.authentication_service.model.UserInfo;

public class EmailExistedException extends BusinessException {
    public EmailExistedException() {
        super(ServiceError.CMN_EMAIL_EXISTED, null, null);
    }
}
