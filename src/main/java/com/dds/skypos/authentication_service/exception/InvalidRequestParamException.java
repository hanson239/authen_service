package com.dds.skypos.authentication_service.exception;


import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class InvalidRequestParamException extends BusinessException {
    public InvalidRequestParamException() {
        super(ServiceError.CMN_INVALID_PARAM, null, null);
    }
}
