package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class InvalidAccessTokenException extends BusinessException {
    public InvalidAccessTokenException() {
        super(ServiceError.INVALID_ACCESS_TOKEN, null, null);
    }
}
