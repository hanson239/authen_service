package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class DomainExistedException extends BusinessException {
    public DomainExistedException() {
        super(ServiceError.CMN_DOMAIN_EXISTED, null, null);
    }
}
