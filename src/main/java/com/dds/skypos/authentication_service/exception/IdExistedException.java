package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;
import com.dds.skypos.authentication_service.model.UserInfo;

public class IdExistedException extends BusinessException {
    public IdExistedException() {
        super(ServiceError.CMN_ID_EXISTED, null, null);
    }
}
