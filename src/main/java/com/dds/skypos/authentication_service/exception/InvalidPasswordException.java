package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class InvalidPasswordException extends BusinessException {
    public InvalidPasswordException() {
        super(ServiceError.INVALID_PASSWORD, null, null);
    }
}
