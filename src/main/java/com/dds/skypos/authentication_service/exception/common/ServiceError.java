package com.dds.skypos.authentication_service.exception.common;

import lombok.Getter;

@Getter
public enum ServiceError {
    INVALID_ACCESS_TOKEN(401, "err.authorize.invalid-access-token"),
    INVALID_AUTHORIZATION_TOKEN(401, "err.authorize.invalid-authorization-token"),
    INVALID_ACCESS_TOKEN_FORMAT(401, "err.authorize.invalid-access-token-format"),
    INVALID_AUTHORIZATION_TOKEN_FORMAT(401, "err.authorize.invalid-authorization-token-format"),
    INVALID_OTP(401, "err.authorize.invalid-otp"),
    INVALID_DOMAIN(401, "err.authorize.invalid-domain"),
    INVALID_PASSWORD(401, "err.authorize.invalid-password"),
    INVALID_RESET_PASSWORD_TOKEN(401, "err.authorize.invalid-reset-password-token"),
    ACCESS_TOKEN_INFO_INCORRECT(401, "err.authorize.access-token-info-incorrect"),
    AUTHORIZATION_TOKEN_INFO_INCORRECT(401, "err.authorize.authorization-token-info-incorrect"),
    SESSION_EXPIRED(401, "err.authorize.session-expired"),
    CMN_ACCESS_DENIED(401, "err.authorize.access-denied"),
    CMN_INVALID_PARAM(400, "err.api.invalid-param"),
    CMN_ID_EXISTED(400, "err.api.id-existed"),
    CMN_EMAIL_EXISTED(400, "err.api.email-existed"),
    CMN_DOMAIN_EXISTED(400, "err.api.domain-existed"),
    PASSWORD_NOT_MATCH(400,"err.api.password-not-match"),
    PASSWORD_USED(400,"err.api.password-used"),
    CMN_HTTP_METHOD_NOT_SUPPORT(405, "err.api.http-method-not-support"),
    CMN_MEDIA_TYPE_NOT_SUPPORT(415, "err.api.media-type-not-support"),
    CMN_MESSAGE_NOT_READABLE(400, "err.api.message-not-readable"),
    ENTITY_NOT_FOUND(404, "err.api.entity-not-found"),
    UNEXPECTED_EXCEPTION(500, "err.sys.unexpected-exception");

    ServiceError(int errCode, String messageKey) {
        this.errCode = errCode;
        this.messageKey = messageKey;
    }

    private final int errCode;
    private final String messageKey;
}
