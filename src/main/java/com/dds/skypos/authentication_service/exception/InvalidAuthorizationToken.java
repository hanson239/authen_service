package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class InvalidAuthorizationToken extends BusinessException {
    protected InvalidAuthorizationToken() {
        super(ServiceError.INVALID_AUTHORIZATION_TOKEN, null, null);
    }
}
