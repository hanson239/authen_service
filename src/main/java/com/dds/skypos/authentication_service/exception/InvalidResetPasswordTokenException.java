package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class InvalidResetPasswordTokenException extends BusinessException {
    public InvalidResetPasswordTokenException() {
        super(ServiceError.INVALID_RESET_PASSWORD_TOKEN, null, null);
    }
}
