package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class InvalidOTPException extends BusinessException {
    public InvalidOTPException() {
        super(ServiceError.INVALID_OTP, null, null);
    }
}
