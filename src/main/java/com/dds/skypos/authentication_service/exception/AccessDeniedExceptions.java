package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class AccessDeniedExceptions extends BusinessException {
    public AccessDeniedExceptions() {
        super(ServiceError.CMN_ACCESS_DENIED, null, null);
    }
}
