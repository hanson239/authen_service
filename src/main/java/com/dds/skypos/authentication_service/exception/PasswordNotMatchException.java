package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class PasswordNotMatchException extends BusinessException {
    public PasswordNotMatchException() {
        super(ServiceError.PASSWORD_NOT_MATCH, null, null);
    }
}
