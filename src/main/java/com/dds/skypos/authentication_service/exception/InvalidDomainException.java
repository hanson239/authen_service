package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class InvalidDomainException extends BusinessException {
    public InvalidDomainException() {
        super(ServiceError.INVALID_DOMAIN, null, null);
    }
}
