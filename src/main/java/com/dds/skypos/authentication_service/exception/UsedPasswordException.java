package com.dds.skypos.authentication_service.exception;

import com.dds.skypos.authentication_service.exception.common.BusinessException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;

public class UsedPasswordException extends BusinessException {
    public UsedPasswordException() {
        super(ServiceError.PASSWORD_USED, null, null);
    }
}
