package com.dds.skypos.authentication_service.aop.log.activity;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.slf4j.MDC;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import ua_parser.Client;
import ua_parser.Parser;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.nonNull;

@Slf4j
@Component
public class MdcLogEnhancerFilter extends GenericFilterBean {

    // MDC key
    private static class MdcKey {
        private static final String REQUEST_ID = "requestId";
        private static final String URL = "requestUrl";
        private static final String IP = "clientIp";
        private static final String IP_TEXT = "clientIpText";
        private static final String ORIGIN = "requestOrigin";
        private static final String LANGUAGE = "lang";
        private static final String EXPIRE_HEADER = "requestExpires";

        private static final String USER_AGENT_FAMILY = "agentFamily";
        private static final String USER_AGENT_MAJOR = "agentMajor";
        private static final String USER_AGENT_MINOR = "agentMinor";
        private static final String USER_AGENT_PATCH = "agentPatch";
        private static final String CLIENT_OS_FAMILY = "clientOsFamily";
        private static final String CLIENT_OS_MAJOR = "clientOsMajor";
        private static final String CLIENT_OS_MINOR = "clientOsMinor";
        private static final String CLIENT_OS_PATCH = "clientOsPatch";
        private static final String CLIENT_OS_PATCH_MINOR = "clientOsPatchMinor";
        private static final String DEVICE_TYPE_FAMILY = "deviceTypeFamily";

        private static final String LOGIN_ID = "loginId";
        private static final String USER_ID = "userId";
        private static final String USERNAME = "userName";
        private static final String USER_LANG = "userLang";
     }

    // default value for some key
    private static final String UNAUTHORIZED_USER = "unauthorized";

    // http header
    private static final String FORWARD_IP_HEADER_OFFICIAL_STANDARD = "Forwarded";
    private static final String[] FORWARD_IP_HEADER_CANDIDATES = {
        "X-Forwarded-For",
        "Proxy-Client-IP",
        "WL-Proxy-Client-IP",
        "HTTP_X_FORWARDED_FOR",
        "HTTP_X_FORWARDED",
        "HTTP_X_CLUSTER_CLIENT_IP",
        "HTTP_CLIENT_IP",
        "HTTP_FORWARDED_FOR",
        "HTTP_FORWARDED",
        "HTTP_VIA",
        "REMOTE_ADDR"
    };

    private final InetAddressValidator inetAddressValidator = InetAddressValidator.getInstance();
    private final Parser uaParser = new Parser();

    public MdcLogEnhancerFilter() throws IOException {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        try {
            insertIntoMDC(httpServletRequest);
            chain.doFilter(request, response);
        } finally {
            clearMDC();
        }
    }

    private void insertIntoMDC(HttpServletRequest request) {
        try {
            insertRequestInfo(request);
            insertClientInfoIntoMdc(request);
//            insertUserInfoIntoMdc();
        } catch (Exception ex) {
            log.warn("Fail to parse client information: {}", ex.getMessage());
            clearMDC();
        }
    }

    private void insertRequestInfo(HttpServletRequest request) {
        addMDC(MdcKey.REQUEST_ID, UUID.randomUUID().toString().toUpperCase().replace("-", ""));
        addMDC(MdcKey.URL, request.getRequestURI());
        String ip = getIp(request);
        if (inetAddressValidator.isValid(ip)) {
            addMDC(MdcKey.IP, ip);
        } else {
            addMDC(MdcKey.IP_TEXT, ip);
        }
        addMDC(MdcKey.ORIGIN, request.getHeader(HttpHeaders.ORIGIN));
//        addMDC(LONGITUDE, request.getHeader(GatConstant.CustomHttpHeader.GAT_LONGITUDE));
//        addMDC(LATITUDE, request.getHeader(GatConstant.CustomHttpHeader.GAT_LATITUDE));
        addMDC(MdcKey.LANGUAGE, request.getHeader(HttpHeaders.ACCEPT_LANGUAGE));
        addMDC(MdcKey.EXPIRE_HEADER, request.getHeader(HttpHeaders.EXPIRES));
    }

    private void insertClientInfoIntoMdc(HttpServletRequest request) {
        String userAgent = request.getHeader(HttpHeaders.USER_AGENT);
        if (nonNull(userAgent)) {
            Client uaClient = uaParser.parse(userAgent);
            addMDC(MdcKey.USER_AGENT_FAMILY, uaClient.userAgent.family);
            addMDC(MdcKey.USER_AGENT_MAJOR, uaClient.userAgent.major);
            addMDC(MdcKey.USER_AGENT_MINOR, uaClient.userAgent.minor);
            addMDC(MdcKey.USER_AGENT_PATCH, uaClient.userAgent.patch);
            addMDC(MdcKey.CLIENT_OS_FAMILY, uaClient.os.family);
            addMDC(MdcKey.CLIENT_OS_MAJOR, uaClient.os.major);
            addMDC(MdcKey.CLIENT_OS_MINOR, uaClient.os.minor);
            addMDC(MdcKey.CLIENT_OS_PATCH, uaClient.os.patch);
            addMDC(MdcKey.CLIENT_OS_PATCH_MINOR, uaClient.os.patchMinor);
            addMDC(MdcKey.DEVICE_TYPE_FAMILY, uaClient.device.family);
        } else {
            addMDC(MdcKey.USER_AGENT_FAMILY, StringUtils.EMPTY);
            addMDC(MdcKey.USER_AGENT_MAJOR, StringUtils.EMPTY);
            addMDC(MdcKey.USER_AGENT_MINOR, StringUtils.EMPTY);
            addMDC(MdcKey.USER_AGENT_PATCH, StringUtils.EMPTY);
            addMDC(MdcKey.CLIENT_OS_FAMILY, StringUtils.EMPTY);
            addMDC(MdcKey.CLIENT_OS_MAJOR, StringUtils.EMPTY);
            addMDC(MdcKey.CLIENT_OS_MINOR, StringUtils.EMPTY);
            addMDC(MdcKey.CLIENT_OS_PATCH, StringUtils.EMPTY);
            addMDC(MdcKey.CLIENT_OS_PATCH_MINOR, StringUtils.EMPTY);
            addMDC(MdcKey.DEVICE_TYPE_FAMILY, StringUtils.EMPTY);
        }
    }

//    private void insertUserInfoIntoMdc() {
//        securityContextManager.getCurrentLoginSession()
//                .ifPresentOrElse(
//                    this::insertAuthUsernameIntoMdc,
//                    this::insertAnonymousUserIntoMdc
//                );
//    }
//
//    private void insertAuthUsernameIntoMdc(UserLoginSession userLoginSession) {
//        addMDC(MdcKey.LOGIN_ID, userLoginSession.getLoginId().toString());
//        addMDC(MdcKey.USER_ID, userLoginSession.getUserInfo().getUserId().toString());
//        addMDC(MdcKey.USERNAME, userLoginSession.getUserInfo().getName());
//        addMDC(MdcKey.USER_LANG, userLoginSession.getAcceptLanguage());
//    }

    private void insertAnonymousUserIntoMdc() {
        addMDC(MdcKey.LOGIN_ID, StringUtils.EMPTY);
        addMDC(MdcKey.USERNAME, UNAUTHORIZED_USER);
        addMDC(MdcKey.USER_LANG, StringUtils.EMPTY);
    }

    private String getIp(final HttpServletRequest request) {
        return getForwardedIpFromStandardHeader(request)
                .or(() -> getForwardedIpFromNonStandardHeader(request))
                .orElse(request.getRemoteAddr());
    }

    private Optional<String> getForwardedIpFromStandardHeader(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(FORWARD_IP_HEADER_OFFICIAL_STANDARD))
                .map(this::parseForwardedIpStandardHeader);
    }

    private String parseForwardedIpStandardHeader(String headerValue) {
        String normalizedValue = StringUtils.normalizeSpace(headerValue);
        String[] attributes = normalizedValue.split(";");
        return Arrays.stream(attributes)
                .map(String::trim)
                .filter(v -> v.contains("for"))
                .findFirst()
                .map(v -> {
                    Pattern pattern = Pattern.compile("for=(?<ip>.*)$");
                    Matcher matcher = pattern.matcher(v);
                    if (matcher.matches()) {
                        return matcher.group("ip");
                    } else {
                        return null;
                    }
                }).orElse(null);
    }

    private Optional<String> getForwardedIpFromNonStandardHeader(HttpServletRequest request) {
        return Arrays.stream(FORWARD_IP_HEADER_CANDIDATES)
                .filter(h -> StringUtils.isNotBlank(request.getHeader(h)))
                .map(this::parseForwardedIpNonStandardHeader)
                .filter(Objects::nonNull)
                .findFirst();
    }

    private String parseForwardedIpNonStandardHeader(String headerValue) {
        Pattern pattern = Pattern.compile("for=(?<ip>.*)$");
        Matcher matcher = pattern.matcher(headerValue);
        if (matcher.matches()) {
            return matcher.group("ip");
        } else {
            String ip = headerValue.split(",")[0];
            if (inetAddressValidator.isValid(ip)) {
                return ip;
            } else {
                return null;
            }
        }
    }

    private void addMDC(String key, String value) {
        MDC.put(reformatKey(key), reformatValue(key, value));
    }

    private String reformatKey(String key) {
        return key;
    }

    private String reformatValue(String key, String value) {
        return value;
    }

    private void clearMDC() {
        MDC.remove(MdcKey.URL);
        MDC.remove(MdcKey.IP);
        MDC.remove(MdcKey.IP_TEXT);
        MDC.remove(MdcKey.ORIGIN);
//        MDC.remove(LONGITUDE);
//        MDC.remove(LATITUDE);
        MDC.remove(MdcKey.LANGUAGE);
        MDC.remove(MdcKey.EXPIRE_HEADER);

        MDC.remove(MdcKey.USER_AGENT_FAMILY);
        MDC.remove(MdcKey.USER_AGENT_MAJOR);
        MDC.remove(MdcKey.USER_AGENT_MINOR);
        MDC.remove(MdcKey.USER_AGENT_PATCH);
        MDC.remove(MdcKey.CLIENT_OS_FAMILY);
        MDC.remove(MdcKey.CLIENT_OS_MAJOR);
        MDC.remove(MdcKey.CLIENT_OS_MINOR);
        MDC.remove(MdcKey.CLIENT_OS_PATCH);
        MDC.remove(MdcKey.CLIENT_OS_PATCH_MINOR);
        MDC.remove(MdcKey.DEVICE_TYPE_FAMILY);

        MDC.remove(MdcKey.LOGIN_ID);
        MDC.remove(MdcKey.USER_ID);
        MDC.remove(MdcKey.USERNAME);
        MDC.remove(MdcKey.USER_LANG);
    }
}
