package com.dds.skypos.authentication_service.controller;

import com.dds.skypos.authentication_service.dto.req.*;
import com.dds.skypos.authentication_service.dto.res.LoginDTO;
import com.dds.skypos.authentication_service.dto.res.ResetPasswordTokenDTO;
import com.dds.skypos.authentication_service.dto.res.UserDTO;
import com.dds.skypos.authentication_service.response.Response;
import com.dds.skypos.authentication_service.response.ResponseUtils;
import com.dds.skypos.authentication_service.service.PosAuthenticationService;
import com.dds.skypos.authentication_service.service.component.PagingReq;
import com.dds.skypos.authentication_service.service.component.PagingRes;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/skypos/auth")
public class PosAuthenticationController {

    private final PosAuthenticationService posAuthenticationService;

    @ApiOperation(value = "User send register info to get OTP via email")
    @PostMapping({"/register"})
    public ResponseEntity<Response> preRegister(@Valid @RequestBody PreRegisterAccountDTO requestDTO){
        posAuthenticationService.preRegister(requestDTO);
        return ResponseUtils.ok();
    }

    @ApiOperation(value = "User send register info and OTP to create account")
    @PostMapping({"/register/otp"})
    public ResponseEntity<Response> register(@Valid @RequestBody RegisterAccountDTO requestDTO){
        return ResponseUtils.ok(posAuthenticationService.register(requestDTO));
    }

    @ApiOperation(value = "User login by email to get access token", response = LoginDTO.class)
    @PostMapping({"/login_by_email"})
    public ResponseEntity<Response> loginByEmail(@Valid @RequestBody LoginByEmailDTO requestDTO) {
        return ResponseUtils.ok(posAuthenticationService.loginByEmail(requestDTO));
    }


    @ApiOperation(value = "Store Admin, Cashier login in Pos by NumberID to get access token", response = LoginDTO.class)
    @PostMapping({"/login_by_id"})
    public ResponseEntity<Response> loginById(@Valid @RequestBody LoginByIdDTO requestDTO) {
        return ResponseUtils.ok(posAuthenticationService.loginById(requestDTO));
    }

    @PreAuthorize("hasAnyRole('STORE_ACCOUNT', 'STORE_ADMIN')")
    @ApiOperation(value = "Create Empoyee", response = UserDTO.class)
    @PostMapping({"/employee"})
    public ResponseEntity<Response> createEmployee(@Valid @RequestBody EmployeeCreateDTO requestDTO) {
        return ResponseUtils.ok(posAuthenticationService.createEmployee(requestDTO));
    }

    @PreAuthorize("hasAnyRole('STORE_ACCOUNT', 'STORE_ADMIN')")
    @ApiOperation(value = "Delete Employee ")
    @DeleteMapping("/employee/{numberId}")
    public ResponseEntity<Response> delete(@PathVariable("numberId") Integer numberId) {
        posAuthenticationService.deleteEmployee(numberId);
        return ResponseUtils.ok(StringUtils.EMPTY);
    }

    @PreAuthorize("hasAnyRole('STORE_ACCOUNT', 'STORE_ADMIN')")
    @ApiOperation(value = "Search Employee", response = UserDTO.class)
    @GetMapping({"/employee"})
    public ResponseEntity<PagingRes> list(@Valid EmployeeSearchDTO employeeSearchDTO,
                                          @Validated PagingReq req) {
        return ResponseEntity.ok(PagingRes.of(posAuthenticationService.searchEmployee(employeeSearchDTO, req.makePageable())));
    }

    @PreAuthorize("hasAnyRole('STORE_ACCOUNT', 'STORE_ADMIN')")
    @ApiOperation(value = "Update Employee Info", response = UserDTO.class)
    @PutMapping({"/employee/{numberId}"})
    public ResponseEntity<Response> update(@PathVariable("numberId") Integer numberId,
                                           @RequestBody EmployeeUpdateDTO requestDTO) {
        return ResponseUtils.ok(posAuthenticationService.updateEmployee(requestDTO, numberId));
    }

    @ApiOperation(value = "User logout")
    @PostMapping({"/logout"})
    public ResponseEntity<Response> logout() {
        posAuthenticationService.logout();
        return ResponseUtils.ok();
    }

    @ApiOperation(value = "Store Account Logout from Pos")
    @PostMapping({"/logout_pos"})
    public ResponseEntity<Response> storeAccountLogoutFromPos(@Valid @RequestBody StoreAccountLogoutFromPosDTO requestDTO) {
        posAuthenticationService.storeAccountLogoutFromPos(requestDTO);
        return ResponseUtils.ok();
    }

    @ApiOperation(value = "User change password")
    @PutMapping({"/password"})
    public ResponseEntity<Response> changePassword(@Valid @RequestBody ChangePasswordDTO requestDTO) {
        posAuthenticationService.changePassword(requestDTO);
        return ResponseUtils.ok();
    }

    @ApiOperation(value = "User send request forgot password to get OTP via email")
    @PostMapping({"/forgot_password"})
    public ResponseEntity<Response> forgotPassword(@Valid @RequestBody ForgotPasswordDTO requestDTO) {
        posAuthenticationService.forgotPassword(requestDTO);
        return ResponseUtils.ok();
    }

    @ApiOperation(value = "User send OTP and get reset password token", response = ResetPasswordTokenDTO.class)
    @PostMapping({"/otp"})
    public ResponseEntity<Response> validateOTPResetPassword(@Valid @RequestBody ValidateOTPResetPasswordDTO requestDTO) {
        return ResponseUtils.ok(posAuthenticationService.validateOTPResetPassword(requestDTO));
    }

    @ApiOperation(value = "User send new password and token to change password")
    @PutMapping({"/reset_password"})
    public ResponseEntity<Response> changePassword(@Valid @RequestBody ResetPasswordDTO requestDTO) {
        posAuthenticationService.resetPassword(requestDTO);
        return ResponseUtils.ok();
    }

    @ApiOperation(value = "Get authorize token")
    @GetMapping({"/authorize_token"})
    public ResponseEntity<Response> authorizationToken() {
        return ResponseUtils.ok(posAuthenticationService.generateAuthorizationToken());
    }

}
