package com.dds.skypos.authentication_service.dto.res;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResetPasswordTokenDTO {

    @ApiModelProperty("Require in API Change Password, expires after 3 minutes")
    private String resetPasswordToken;

}
