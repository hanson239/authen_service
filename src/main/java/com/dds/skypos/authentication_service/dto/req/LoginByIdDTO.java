package com.dds.skypos.authentication_service.dto.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class LoginByIdDTO {

    @NotBlank
    private String domain;
    @NotBlank
    private String storeCode;
    @NotBlank
    private Integer numberId;
    @NotBlank
    private String passCode;
    @NotNull
    private DeviceLoginDTO device;

}
