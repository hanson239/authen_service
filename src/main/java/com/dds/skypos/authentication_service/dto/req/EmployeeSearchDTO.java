package com.dds.skypos.authentication_service.dto.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class EmployeeSearchDTO {

    private String keySearch;
    @NotBlank
    private String userRole;

}
