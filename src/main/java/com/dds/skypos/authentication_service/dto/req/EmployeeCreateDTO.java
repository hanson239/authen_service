package com.dds.skypos.authentication_service.dto.req;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class EmployeeCreateDTO {

    @NotNull
    private Integer numberId;
    @NotBlank
    private String passCode;
    @Email
    private String email;
    @NotBlank
    private String password;
    @NotBlank
    private String fullName;

    private String userRole;
    @NotBlank
    private String phoneNumber;

}
