package com.dds.skypos.authentication_service.dto.req;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class ValidateOTPResetPasswordDTO {

    @NotBlank
    private String domain;
    @Email
    private String email;
    @Size(min = 5, max = 5)
    private String otp;
    
}
