package com.dds.skypos.authentication_service.dto.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class StoreAccountLogoutFromPosDTO {

    @NotBlank
    private String password;

}
