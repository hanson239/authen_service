package com.dds.skypos.authentication_service.dto.req;

import lombok.Data;

import javax.validation.constraints.Email;

@Data
public class EmployeeUpdateDTO {
    private Integer numberId;
    private String domain;
    private String fullName;
    private String storeCodeForCashier;
    @Email
    private String email;
    private String phoneNumber;
}
