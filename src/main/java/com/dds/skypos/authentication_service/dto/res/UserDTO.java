package com.dds.skypos.authentication_service.dto.res;

import lombok.Data;

import java.time.Instant;

@Data
public class UserDTO {

    private String id;
    private Integer numberId;
    private String fullName;
    private String email;
    private String phoneNumber;
    private Boolean deleteFlag;
    private String role;
    private Instant createdDate;
    private Instant modifiedDate;

}
