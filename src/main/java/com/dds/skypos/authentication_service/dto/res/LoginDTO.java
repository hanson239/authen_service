package com.dds.skypos.authentication_service.dto.res;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class LoginDTO {

    private String accessToken;
    private String role;

}
