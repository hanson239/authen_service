package com.dds.skypos.authentication_service.dto.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginByEmailDTO {

    @NotBlank
    private String domain;
    @NotBlank
    private String email;
    @NotBlank
    private String password;

}
