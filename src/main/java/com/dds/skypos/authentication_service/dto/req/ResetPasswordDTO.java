package com.dds.skypos.authentication_service.dto.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class ResetPasswordDTO {

    @NotBlank
    private String domain;
    @NotBlank
    @Size(min = 8)
    private String newPassword;
    @ApiModelProperty("Token receive after validate OTP Reset Password")
    @NotBlank
    private String resetPasswordToken;

}
