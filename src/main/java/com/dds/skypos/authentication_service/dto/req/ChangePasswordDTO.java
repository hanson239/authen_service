package com.dds.skypos.authentication_service.dto.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class ChangePasswordDTO {

    @NotBlank
    @Size(min = 8)
    private String currentPassword;
    @NotBlank
    @Size(min = 8)
    private String newPassword;

}
