package com.dds.skypos.authentication_service.dto.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class ForgotPasswordDTO {

    @NotBlank
    private String domain;
    @NotBlank
    private String email;

}
