package com.dds.skypos.authentication_service.dto.req;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DeviceLoginDTO {

    @NotBlank
    private String uuid;
    @NotBlank
    private String macAddress;
    @NotBlank
    private String deviceOS;

}
