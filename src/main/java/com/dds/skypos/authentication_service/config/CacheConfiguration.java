package com.dds.skypos.authentication_service.config;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableCaching
public class CacheConfiguration {

    @UtilityClass
    public static class CacheName {
    }


    @Slf4j
    public static class CacheEventLogger
            implements CacheEventListener<Object, Object> {
        @Override
        public void onEvent(
                CacheEvent<? extends Object, ? extends Object> cacheEvent) {
            log.debug("Key: {} | EventType: {} | Old value: {} | New value: {}",
                    cacheEvent.getKey(), cacheEvent.getType(), cacheEvent.getOldValue(),
                    cacheEvent.getNewValue());
        }
    }
}
