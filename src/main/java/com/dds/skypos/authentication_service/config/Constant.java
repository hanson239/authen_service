package com.dds.skypos.authentication_service.config;

public class Constant {

    public static class Profile {
        public static final String DEV = "dev";
        public static final String PROD = "prod";
        public static final String CRON = "cron";
    }

    /**
     * Timezone +7:00
     */
    public static final String SYSTEM_LOCAL_TIMEZONE = "Asia/Ho_Chi_Minh";
}
