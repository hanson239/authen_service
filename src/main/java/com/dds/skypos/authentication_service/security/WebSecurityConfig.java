package com.dds.skypos.authentication_service.security;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public CustomAccessDeniedHandler customAccessDeniedHandler() {
        return new CustomAccessDeniedHandler();
    }

    @Bean
    public JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter() throws Exception {
        JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter = new JwtAuthenticationTokenFilter();
        jwtAuthenticationTokenFilter.setAuthenticationManager(authenticationManager());
        return jwtAuthenticationTokenFilter;
    }

    @Bean
    public RestAuthenticationEntryPoint restServicesEntryPoint() {
        return new RestAuthenticationEntryPoint();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
            .cors()
            .and().authorizeRequests()
                .antMatchers("/api/skypos/auth/login_by_email").permitAll()
                .antMatchers("/api/skypos/auth/login_by_id").permitAll()
                .antMatchers("/api/skypos/auth/forgot_password").permitAll()
                .antMatchers("/api/skypos/auth/reset_password").permitAll()
                .antMatchers("/api/skypos/auth/otp").permitAll()
                .antMatchers("/api/skypos/auth/register").permitAll()
                .antMatchers("/api/skypos/auth/register/otp").permitAll()
                .antMatchers("/api/**").authenticated()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().addFilterBefore(jwtAuthenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling().accessDeniedHandler(customAccessDeniedHandler());
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers("/resources/**")
                .antMatchers("/webjars/**")//
                .antMatchers("/public");
    }

    @Configuration
    @Order(2)
    @Profile("dev")
    public static class SwaggerSecurityConfig extends WebSecurityConfigurerAdapter {

        private static final String DEV_ROLE = "ROLE_DEV";
        @Value("${swagger.user}")
        private String swaggerUser;
        @Value("${swagger.password}")
        private String swaggerPassword;

        // @formatter:off
        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.antMatcher("/swagger**")
                    .httpBasic()
                    .authenticationEntryPoint(swaggerAuthenticationEntryPoint())
                    .and()
                    .authorizeRequests()
                    .anyRequest().authenticated();
        }
        // @formatter:on

        @Bean
        public BasicAuthenticationEntryPoint swaggerAuthenticationEntryPoint() {
            BasicAuthenticationEntryPoint entryPoint = new BasicAuthenticationEntryPoint();
            entryPoint.setRealmName("Swagger Realm");
            return entryPoint;
        }

        @Override
        public void configure(AuthenticationManagerBuilder auth) throws Exception {
            final PasswordEncoder passwordEncoder = passwordEncoder();
            auth.inMemoryAuthentication()
                    .passwordEncoder(passwordEncoder)
                    .withUser(swaggerUser)
                    .password(passwordEncoder.encode(swaggerPassword))
                    .authorities(DEV_ROLE);
        }

        public PasswordEncoder passwordEncoder() {
            return new BCryptPasswordEncoder();
        }

        @Override
        public void configure(WebSecurity web) {
            // Allow swagger to be accessed without authentication
            web.ignoring()
                    .antMatchers("/resources/**")
                    .antMatchers("/webjars/**")//
                    .antMatchers("/public");
        }
    }
}
