package com.dds.skypos.authentication_service.security;

import com.dds.skypos.authentication_service.exception.EntityNotFoundException;
import com.dds.skypos.authentication_service.exception.common.ServiceError;
import com.dds.skypos.authentication_service.exception.common.SysException;
import com.dds.skypos.authentication_service.model.LoginInfo;
import com.dds.skypos.authentication_service.model.UserInfo;
import com.dds.skypos.authentication_service.repository.UserInfoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.swing.text.html.parser.Entity;
import java.util.Optional;

@RequiredArgsConstructor
@Component
public class SecurityContextManager {

    private final UserInfoRepository userInfoRepository;

    public Optional<LoginInfo> resolveAuthentication(){
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .filter(authentication -> authentication.getPrincipal() instanceof LoginInfo)
                .map(authentication -> (LoginInfo) authentication.getPrincipal());
    }

    public LoginInfo getCurrentLoginInfo(){
        return resolveAuthentication()
                .orElseThrow(() -> new SysException(ServiceError.SESSION_EXPIRED.getMessageKey()));
    }

    public UserInfo getCurrentLoginUserInfo(){
        return userInfoRepository.findById(
                getCurrentLoginInfo().getUserLogin().getId()
        ).orElseThrow(EntityNotFoundException::new);
    }

}
