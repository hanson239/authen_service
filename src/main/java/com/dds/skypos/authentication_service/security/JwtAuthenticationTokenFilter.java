package com.dds.skypos.authentication_service.security;

import com.dds.skypos.authentication_service.model.LoginInfo;
import com.dds.skypos.authentication_service.service.PosAuthenticationService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class JwtAuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {

    private final static String ACCESS_TOKEN_HEADER = "access-token";
    @Autowired
    private PosAuthenticationService POSAuthenticationService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        String accessToken = ((HttpServletRequest) request).getHeader(ACCESS_TOKEN_HEADER);
        if (StringUtils.isNotBlank(accessToken)) {
            LoginInfo loginInfo = POSAuthenticationService.parseAccessToken(accessToken);
            SecurityContextHolder.getContext().setAuthentication(
                    new UsernamePasswordAuthenticationToken(
                            loginInfo,
                            loginInfo.getUserLogin().getEmail(),
                            loginInfo.getUserLogin().getAuthorities()
                    )
            );
        }
        chain.doFilter(request, response);
    }
}
