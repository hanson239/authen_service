package utils;

import com.dds.skypos.authentication_service.config.Constant;
import lombok.experimental.UtilityClass;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;

@UtilityClass
public class SystemTimeUtils {
    public final String DATE_FORMAT = "yyyyMMdd";
    public final String MONTH_FORMAT = "yyyyMM";
    public final String YEAR_FORMAT = "yyyy";
    public final String WEEK_FORMAT = "%s_%s";
    public final String QUARTER_FORMAT = "%s_%s";
    public final String DEFAULT_DISPLAY_FULL_TIME_FORMAT = "yyyy/MM/dd - HH:mm:ss Z";

    public class Quarter {
        public static final String Q1 = "Q1";
        public static final String Q2 = "Q2";
        public static final String Q3 = "Q3";
        public static final String Q4 = "Q4";
    }
    public ZonedDateTime getLocalCurrentDate() {
        return getLocalDate(Instant.now());
    }

    public ZonedDateTime getLocalCurrentTime() {
        return getLocalTime(Instant.now());
    }

    public ZonedDateTime getLocalCurrentMonthStartDate() {
        return getLocalMonthStartDate(Instant.now());
    }

    public ZonedDateTime getLocalCurrentMonthEndDate() {
        return getLocalMonthEndDate(Instant.now());
    }

    public ZonedDateTime getLocalCurrentWeekStartDate() {
        return getLocalWeekStartDate(Instant.now());
    }

    public ZonedDateTime getLocalCurrentWeekEndDate() {
        return getLocalWeekEndDate(Instant.now());
    }

    public ZonedDateTime getLocalCurrentQuarterStartDate() {
        return getLocalQuarterStartDate(Instant.now());
    }

    public ZonedDateTime getLocalCurrentQuarterEndDate() {
        return getLocalQuarterEndDate(Instant.now());
    }

    public ZonedDateTime getLocalCurrentYearStartDate() {
        return getLocalYearStartDate(Instant.now());
    }

    public ZonedDateTime getLocalCurrentYearEndDate() {
        return getLocalYearEndDate(Instant.now());
    }

    public ZonedDateTime getLocalTime(Instant utcTime) {
        return utcTime.atZone(ZoneId.of(Constant.SYSTEM_LOCAL_TIMEZONE));
    }

    public ZonedDateTime getLocalDate(Instant utcTime) {
        return getLocalTime(utcTime).truncatedTo(ChronoUnit.DAYS);
    }

    public ZonedDateTime getLocalMonthStartDate(Instant utcTime) {
        return getLocalDate(utcTime).with(TemporalAdjusters.firstDayOfMonth());
    }

    public ZonedDateTime getLocalMonthEndDate(Instant utcTime) {
        return getLocalMonthStartDate(utcTime).with(TemporalAdjusters.lastDayOfMonth());
    }

    public ZonedDateTime getLocalWeekStartDate(Instant utcTime) {
        return getLocalDate(utcTime).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
    }

    public ZonedDateTime getLocalWeekEndDate(Instant utcTime) {
        return getLocalDate(utcTime).with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));
    }

    private ZonedDateTime getLocalQuarterStartDate(Instant utcTime) {
        ZonedDateTime localTime = getLocalTime(utcTime);
        String quarter = determineQuarter(localTime);
        ZonedDateTime startQuarterMonth = null;
        switch (quarter) {
            case Quarter.Q1:
                startQuarterMonth = localTime.withMonth(Month.JANUARY.getValue());
                break;
            case Quarter.Q2:
                startQuarterMonth = localTime.withMonth(Month.APRIL.getValue());
                break;
            case Quarter.Q3:
                startQuarterMonth = localTime.withMonth(Month.JULY.getValue());
                break;
            case Quarter.Q4:
            default:
                startQuarterMonth = localTime.withMonth(Month.OCTOBER.getValue());
        }
        return startQuarterMonth
                .truncatedTo(ChronoUnit.DAYS)
                .with(TemporalAdjusters.firstDayOfMonth());
    }

    private ZonedDateTime getLocalQuarterEndDate(Instant utcTime) {
        ZonedDateTime localTime = getLocalTime(utcTime);
        String quarter = determineQuarter(localTime);
        ZonedDateTime endQuarterMonth = null;
        switch (quarter) {
            case Quarter.Q1:
                endQuarterMonth = localTime.withMonth(Month.MARCH.getValue());
                break;
            case Quarter.Q2:
                endQuarterMonth = localTime.withMonth(Month.JUNE.getValue());
                break;
            case Quarter.Q3:
                endQuarterMonth = localTime.withMonth(Month.SEPTEMBER.getValue());
                break;
            case Quarter.Q4:
            default:
                endQuarterMonth = localTime.withMonth(Month.DECEMBER.getValue());
        }
        return endQuarterMonth
                .truncatedTo(ChronoUnit.DAYS)
                .with(TemporalAdjusters.lastDayOfMonth());
    }

    public ZonedDateTime getLocalYearStartDate(Instant utcTime) {
        return getLocalDate(utcTime).with(TemporalAdjusters.firstDayOfYear());
    }

    public ZonedDateTime getLocalYearEndDate(Instant utcTime) {
        return getLocalYearStartDate(utcTime).with(TemporalAdjusters.lastDayOfYear());
    }

    public String formatLocalTime(ZonedDateTime zonedDateTime, String pattern) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
        return zonedDateTime.format(formatter);
    }

    public String formatDateString(Instant utcTime) {
        ZonedDateTime localDate = SystemTimeUtils.getLocalDate(utcTime);
        return SystemTimeUtils.formatLocalTime(localDate, DATE_FORMAT);
    }

    public String formatWeekString(Instant utcTime) {
        ZonedDateTime monday = SystemTimeUtils.getLocalWeekStartDate(utcTime);
        ZonedDateTime sunday = SystemTimeUtils.getLocalWeekEndDate(utcTime);
        return String.format(WEEK_FORMAT,
                SystemTimeUtils.formatLocalTime(monday, DATE_FORMAT),
                SystemTimeUtils.formatLocalTime(sunday, DATE_FORMAT));
    }

    public String formatMonthString(Instant utcTime) {
        ZonedDateTime monthStartDate = SystemTimeUtils.getLocalMonthStartDate(utcTime);
        return SystemTimeUtils.formatLocalTime(monthStartDate, MONTH_FORMAT);
    }

    public String formatQuarterString(Instant utcTime) {
        ZonedDateTime localTime = SystemTimeUtils.getLocalTime(utcTime);
        return String.format(QUARTER_FORMAT,
                SystemTimeUtils.formatLocalTime(localTime, YEAR_FORMAT),
                SystemTimeUtils.determineQuarter(localTime));
    }

    public String formatYearString(Instant utcTime) {
        ZonedDateTime yearStartDate = SystemTimeUtils.getLocalYearStartDate(utcTime);
        return SystemTimeUtils.formatLocalTime(yearStartDate, YEAR_FORMAT);
    }

    public ZonedDateTime parseDateString(String dateString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT + " V");
        return ZonedDateTime.parse(String.format("%s %s", dateString, Constant.SYSTEM_LOCAL_TIMEZONE), formatter);
    }

    public ZonedDateTime parseWeekString(String weekString) {
        String startWeekDate = weekString.split("_")[0];
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT + " V");
        return ZonedDateTime.parse(String.format("%s %s", startWeekDate, Constant.SYSTEM_LOCAL_TIMEZONE), formatter);
    }

    public ZonedDateTime parseMonthString(String monthString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MONTH_FORMAT + " V");
        return ZonedDateTime.parse(String.format("%s %s", monthString, Constant.SYSTEM_LOCAL_TIMEZONE), formatter);
    }

    public ZonedDateTime parseQuarterString(String quarterString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(YEAR_FORMAT + "_Q V");
        return ZonedDateTime.parse(String.format("%s %s", quarterString, Constant.SYSTEM_LOCAL_TIMEZONE), formatter);
    }

    public ZonedDateTime parseYearString(String yearString) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(YEAR_FORMAT + " V");
        return ZonedDateTime.parse(String.format("%s %s", yearString, Constant.SYSTEM_LOCAL_TIMEZONE), formatter);
    }

    /**
     * January, February, and March (Q1)
     * April, May, and June (Q2)
     * July, August, and September (Q3)
     * October, November, and December (Q4)
     * @param startTime day
     * @return quarter
     */
    public String determineQuarter(ZonedDateTime startTime) {
        switch (startTime.getMonth()) {
            case JANUARY:
            case FEBRUARY:
            case MARCH:
                return Quarter.Q1;
            case APRIL:
            case MAY:
            case JUNE:
                return Quarter.Q2;
            case JULY:
            case AUGUST:
            case SEPTEMBER:
                return Quarter.Q3;
            case OCTOBER:
            case NOVEMBER:
            case DECEMBER:
            default:
                return Quarter.Q4;
        }
    }

    public TimeRange getLocalCurrentMonthTimeRange() {
        return getLocalMonthTimeRange(Instant.now());
    }

    public TimeRange getLocalCurrentQuarterTimeRange() {
        return getLocalQuarterTimeRange(Instant.now());
    }

    public TimeRange getLocalCurrentYearTimeRange() {
        return getLocalYearTimeRange(Instant.now());
    }

    public TimeRange getLocalMonthTimeRange(Instant utcTime) {
        return TimeRange.builder()
                .startTime(SystemTimeUtils.getLocalMonthStartDate(utcTime).toInstant())
                .endTime(SystemTimeUtils.getLocalMonthEndDate(utcTime).plusDays(1L).toInstant())
                .build();
    }

    public TimeRange getLocalQuarterTimeRange(Instant utcTime) {
        return TimeRange.builder()
                .startTime(SystemTimeUtils.getLocalQuarterStartDate(utcTime).toInstant())
                .endTime(SystemTimeUtils.getLocalQuarterEndDate(utcTime).plusDays(1L).toInstant())
                .build();
    }

    public TimeRange getLocalYearTimeRange(Instant utcTime) {
        return TimeRange.builder()
                .startTime(SystemTimeUtils.getLocalYearStartDate(utcTime).toInstant())
                .endTime(SystemTimeUtils.getLocalYearEndDate(utcTime).plusDays(1L).toInstant())
                .build();
    }

    public TimeRange parseTimeRangeFromMonthString(String month) {
        return getLocalMonthTimeRange(parseMonthString(month).toInstant());
    }

    public TimeRange parseTimeRangeFromQuarterString(String quarter) {
        return getLocalQuarterTimeRange(parseQuarterString(quarter).toInstant());
    }

    public TimeRange parseTimeRangeFromYearString(String year) {
        return getLocalYearTimeRange(parseYearString(year).toInstant());
    }

    public ZonedDateTime addDate(ZonedDateTime zonedDateTime) {
        return zonedDateTime.plusDays(1L);
    }

    public ZonedDateTime addWeek(ZonedDateTime zonedDateTime) {
        return zonedDateTime.plusDays(7L);
    }

    public ZonedDateTime addMonth(ZonedDateTime zonedDateTime) {
        return zonedDateTime.plusMonths(1L);
    }

    public boolean isAfterToday(Instant endTime) {
        return endTime.isAfter(SystemTimeUtils.getLocalCurrentDate().toInstant());
    }
}
