package utils;

import lombok.Builder;
import lombok.Getter;

import java.time.Instant;

@Getter
@Builder
public class TimeRange {
    private final Instant startTime;
    private final Instant endTime;
}
